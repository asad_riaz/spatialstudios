import React, { useState, useEffect } from "react";
import "./theme.css";
import "./responsive.css";
import "./custom.css";
import Slider from "./Components/Slider";
import Testimonial from "./Components/Testimonial";
import Panaroma from "Components/Panaroma";
import VideoSection from "Components/VideosSection";
import SecondPanaroma from "Components/SecondPanaroma";
import Header from "Components/Header";
import ClientsInfo from "Components/ClientsInfo";
import Contact from "./Components/Contact";
import Footer from "./Components/Footer";
import PostVideoSection from "Components/VideosSection/PostVideoSection";

function App() {
  const [mobile, setMobile] = useState(false);
  const onChangeWidth = () => {
    if (window.innerWidth <= 1230) {
      setMobile(true);
    } else {
      setMobile(false);
    }
  };
  useEffect(() => {
    if (window.innerWidth <= 1230) {
      setMobile(true);
    }
  }, []);
  useEffect(() => {
    window.addEventListener("resize", onChangeWidth);
    return () => window.removeEventListener("resize", onChangeWidth);
  }, []);
  return (
    <React.Fragment>
      <Header />
      <Panaroma />
      <ClientsInfo />
      <SecondPanaroma />
      <Slider />
      <div style={{ position: "relative" }}>
        <VideoSection mobile={mobile} />
        {!mobile && <PostVideoSection mobile={mobile} />}
      </div>
      <div style={{ marginTop: "250px" }}>
        <Testimonial />
      </div>
      <Contact />
      <Footer />
    </React.Fragment>
  );
}

export default App;
