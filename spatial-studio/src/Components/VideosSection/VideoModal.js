import React from "react";
import YoutubeModal from "react-youtube-modal";
import { VideoTitle } from "./Styled";
import Play_Video from "assets/images/play_video.png";
const VideoModal = ({ id, width = "800px" }) => {
  return (
    <YoutubeModal videoId={id} width={width}>
      <VideoTitle type="button" src={Play_Video}>
        Watch Video
      </VideoTitle>
      {/* <button type="button">Open Modal!</button> */}
    </YoutubeModal>
  );
};

export default VideoModal;
