import React from "react";
import "./styles.css";
import ImageGiff from "assets/giffs/Design_My_Space.gif";
import BoxFinder from "assets/giffs/Boxfinder.gif";
import iPhone_X_Mockup from "assets/giffs/iPhone_X_Mockup.png";
import Measure from "assets/giffs/Measure.gif";
import Fast1 from "assets/images/fast_1.svg";
import Fast2 from "assets/images/fast2.svg";
import Fast3 from "assets/images/fast_3.svg";
import VideoModal from "./VideoModal";

const MobileSlider = () => {
  return (
    <div className="mobile-slider">
      <div
        id="carouselExampleSlidesOnly"
        className="carousel slide"
        data-ride="carousel"
      >
        <div className="carousel-inner">
          <div className="carousel-item active">
            <div className="card-content">
              <h2 className="card-title">Box Finder</h2>
              <h3 className="card-description">
                Find the perfect sized box for anything in just seconds
              </h3>
            </div>
            <VideoModal id="WjQifn3AYiU" />
            <div className="mobile-gif">
              <div className="mobile-gif-image">
                <img
                  className="d-block w-100"
                  src={Measure}
                  alt="First slide"
                />
              </div>
              <div className="mobile-gif-showcase">
                <img
                  // className="d-block w-100"
                  src={iPhone_X_Mockup}
                  alt="First slide"
                />
              </div>{" "}
            </div>
          </div>
          <div className="carousel-item">
            <div className="card-content">
              <h2 className="card-title">Design My Space</h2>
              <h3 className="card-description">
                Design any space any way you want
              </h3>
            </div>
            <VideoModal id="DDvbE-Oxvs4" />
            <div className="mobile-gif">
              <div className="mobile-gif-image">
                <img
                  className="d-block w-100"
                  src={BoxFinder}
                  alt="First slide"
                />
              </div>
              <div className="mobile-gif-showcase">
                <img
                  // className="d-block w-100"
                  src={iPhone_X_Mockup}
                  alt="First slide"
                />
              </div>{" "}
            </div>
          </div>
          <div className="carousel-item">
            <div className="card-content">
              <h2 className="card-title">Measure App</h2>
              <h3 className="card-description">
                Measuring a space has never been this easy
              </h3>
            </div>
            <VideoModal id="qpWx4-cTCaQ" />

            <div className="mobile-gif">
              <div className="mobile-gif-image">
                <img
                  className="d-block w-100"
                  src={ImageGiff}
                  alt="First slide"
                />
              </div>
              <div className="mobile-gif-showcase">
                <img
                  // className="d-block w-100"
                  src={iPhone_X_Mockup}
                  alt="First slide"
                />
              </div>{" "}
            </div>
          </div>
        </div>
      </div>
      <div className="mobile-slider-footer">
        <div className="thick-line"></div>
        <div className="company-configuration">
          <div className="company-line">
            <img src={Fast1} />
          </div>
          <div className="company-line">
            <img src={Fast2} />
          </div>
          <div className="company-line">
            <img src={Fast3} />
          </div>
        </div>
        <img />
      </div>
    </div>
  );
};

export default MobileSlider;
