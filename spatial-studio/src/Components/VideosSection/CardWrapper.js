import React from "react";
const CardWrapper = ({ children }) => {
  return <div className="col-lg-4 video-card-wrapper">{children}</div>;
};

export default CardWrapper;
