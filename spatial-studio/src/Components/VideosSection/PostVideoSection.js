import React from "react";
import Fast_company from "assets/images/Fast_company.png";
import Fill_3 from "assets/images/Fill_3.png";
import Fill_5 from "assets/images/Fill_5.png";
import { IndigoDiv, PostVideoWrapper, VideoMergerLeft, VideoMergerRight } from "./Styled";
const PostVideoSection = () => {
  return (
    <PostVideoWrapper src={Fast_company}>
      <VideoMergerLeft src={Fill_3} />
      <IndigoDiv />
      <div className="post-video-logo"></div>
      <VideoMergerRight src={Fill_5} />
    </PostVideoWrapper>
  );
};

export default PostVideoSection;
