import React from "react";
import CardWrapper from "./CardWrapper";
import "./style.css";
import ImageGiff from "assets/giffs/Design_My_Space.gif";
import BoxFinder from "assets/giffs/Boxfinder.gif";
import iPhone_X_Mockup from "assets/giffs/iPhone_X_Mockup.png";
import Measure from "assets/giffs/Measure.gif";
import MobileSlider from "./MobileSlider";
import {
  Card,
  CardContent,
  CardGiffDiv,
  CardTitle,
  CardDescription,
  VideoTitle,
} from "./Styled";
import VideoModal from "./VideoModal";
const VideoSection = ({ mobile }) => {
  return (
    <div id="Portfolio">
      <div className="video-container">
        <div className="solution__label">
          <span className="label label-default video__label--label">
            Our Portfolio
          </span>
        </div>
        <div className="solution__title">
          Individual Solutions for Individual Needs
        </div>
        <div className="solution__content">
          Our highly customizable platform allows you to adapt our state of the
          art solutions to your bespoke needs
        </div>
      </div>
      <div className="row video-row">
        {mobile ? (
          <MobileSlider />
        ) : (
          <>
            <CardWrapper>
              <Card>
                <CardContent>
                  <CardTitle>Box Finder</CardTitle>
                  <CardDescription>
                    Find the perfect sized box for anything in just seconds
                  </CardDescription>
                </CardContent>
                <VideoModal id="WjQifn3AYiU" />
                <CardGiffDiv>
                  <div className="gif">
                    <img src={BoxFinder} alt="rarey" />
                  </div>
                  <div className="mobile-mockup">
                    <img src={iPhone_X_Mockup} alt="rarey" />
                  </div>
                </CardGiffDiv>
              </Card>
            </CardWrapper>
            <CardWrapper>
              <Card>
                <CardContent>
                  <CardTitle>Design My Space</CardTitle>
                  <CardDescription>
                    Design any space any way you want
                  </CardDescription>
                </CardContent>
                <VideoModal id="DDvbE-Oxvs4" />
                <CardGiffDiv>
                  <div className="gif">
                    <img src={ImageGiff} alt="rarey" />
                  </div>
                  <div className="mobile-mockup">
                    <img src={iPhone_X_Mockup} alt="rarey" />
                  </div>
                </CardGiffDiv>
              </Card>
            </CardWrapper>
            <CardWrapper>
              <Card>
                <CardContent>
                  <CardTitle>Measure App</CardTitle>
                  <CardDescription>
                    Measuring a space has never been this easy
                  </CardDescription>
                </CardContent>
                <VideoModal id="qpWx4-cTCaQ" />
                <CardGiffDiv>
                  <div className="gif">
                    <img src={Measure} alt="rarey" />
                  </div>
                  <div className="mobile-mockup">
                    <img src={iPhone_X_Mockup} alt="rarey" />
                  </div>
                </CardGiffDiv>
              </Card>
            </CardWrapper>
          </>
        )}
      </div>
    </div>
  );
};

export default VideoSection;
