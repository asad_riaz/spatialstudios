import styled from "styled-components";
export const Card = styled.div`
  min-width: 300px;
  width: 340px;
  margin: 0 auto;

  /* background: yellow; */
`;

export const VideoTitle = styled.button`
  border: none;
  outline: none;
  letter-spacing: 0.7px;
  background: transparent;
  color: #006fff;
  font-size: 16px;
  text-transform: uppercase;
  margin-bottom: 25px;
  font-weight: 700;
  line-height: 2.375;
  position: relative;
  ::after {
    content: "";
    display: inline-block;
    position: absolute;
    background-image: url(${(p) => (p.src ? p.src : "")});
    background-position: center;
    background-repeat: no-repeat;
    width: 20px;
    top: 8px;
    height: 20px;
  }
  @media (max-width: 490px) {
    font-size: 14px;
    margin-bottom: 5px;
    ::after {
      content: "";
      width: 20px;
      top: 6px;
      height: 20px;
    }
  }
`;
export const CardGiffDiv = styled.div`
  position: relative;
  .gif {
    max-width: 300px;
    background: transparent;
    img {
      height: 562px;
      margin-left: 16px;
      margin-top: 20px;
      border-radius: 0px;
      width: 265px;
    }
    /* height: 50px; */
  }
  .mobile-mockup {
    position: absolute;
    /* left: 30px; */
    top: 0px;
    width: 300px;
    margin-left: -3px;

    img {
      width: 100%;
    }
  }
`;
export const CardContent = styled.div`
  height: 145px;
`;
export const CardTitle = styled.h2`
  font-weight: 700;
  color: #252e3b;
  font-size: 2.8rem;
  margin-bottom: 1.2rem;
  line-height: 1.611;
  letter-spacing: 0.5px;
`;
export const CardDescription = styled.h3`
  /* font-weight: 700; */
  color: #475363;
  font-size: 1.8rem;
  margin-bottom: 1.6rem;
  line-height: 1.611;
  letter-spacing: 0.5px;
`;

export const PostVideoWrapper = styled.div`
  margin-top: 0px;
  position: absolute;
  bottom: 58px;
  width: 100%;
  height: 360px;
  top: 1150px;
  /* bottom: -248px; */
  background: #1b1a40;
  .post-video-logo {
    height: 300px;
    background-image: url(${(p) => (p.src ? p.src : "")});
    background-position: center;
    background-repeat: no-repeat;
  }
  @media (max-width: 1260px) {
    position: relative;
    top: 100px;
  }
  z-index: 55;
`;
export const IndigoDiv = styled.div`
  height: 55px;
  background: #006fff;
`;

export const VideoMergerLeft = styled.div`
  height: 18rem;
  left: 5.5rem;
  width: 9.2rem;
  background-color: transparent;
  position: absolute;
  top: -4rem;
  background-image: url(${(p) => (p.src ? p.src : "")});
  background-position: center;
  background-repeat: no-repeat;
`;
export const VideoMergerRight = styled.div`
  height: 150px;
  /* left: 150px; */
  width: 140px;
  background-color: transparent;
  position: absolute;
  bottom: -80px;
  right: 5px;
  background-image: url(${(p) => (p.src ? p.src : "")});
  background-position: center;
  background-repeat: no-repeat;
`;
