import React from 'react';
import ReactPannellum, { getConfig } from 'react-pannellum';
import PanaromaImage from 'assets/panaromas/panorama1.jpg';
import PanaromaChair from 'assets/cHAIR3.glb';
import backGroundImage from 'assets/1.hdr';
import { TextPanaroma } from './TextPanaroma';
import Licon from 'assets/images/modelIcon.svg';
export default class Example extends React.Component {
  state = {
    pan: '',
  };
  componentDidMount() {
    // fetch(
    //   "https://cdn.glitch.com/96c6905e-6b45-4288-a0a8-209ae58b3bea%2F2710763_LD.glb?v=1593586917200"
    //   )
    //   .then((img) => img.json())
    //   .then((img) => {
    //     console.log(`lalalal`, img)
    //   })
    //   .then((img) => {
    //     let arrayBufferView = new Uint8Array(img);
    //     let blob = new Blob([arrayBufferView], { type: "image/jpg" });
    //     let urlCreator = window.URL || window.webkitURL;
    //     let imageUrl = urlCreator.createObjectURL(blob);
    //     console.log("imgUEl", imageUrl);
    //     this.setState({ pan: imageUrl });
    //   });
  }

  click() {
    console.log('==================================');
    console.log(getConfig());
    console.log('==================================');
  }

  render() {
    const config = {
      autoRotate: -4,
      autoLoad: true,
    };
    // console.log("State:", this.state.pan);
    const modelPath =
      'https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Duck/glTF/Duck.gltf';
    const realPath =
      'https://cdn.glitch.com/96c6905e-6b45-4288-a0a8-209ae58b3bea%2F2710763_LD.glb?v=1593586917200';
    return (
      <div className='zoom-counter' id='What_We_Do'>
        <div className='container-fluid' style={{ position: 'relative' }}>
          <div class='modelViewer--L__icon'>
            <img src={Licon} alt='Licon' />
          </div>
          <div className='zoom-counter__inner zoom-counter__inner--first zoom-counter__inner--tab'>
            <div className='zoom-counter__info'>
              <div className='section__label'>
                <span className='label label-default section__label--label'>
                  What we do
                </span>
              </div>
              <div className='zoom-counter__title' style={{ color: '#242323' }}>
                Your One Stop Shop for Developing, Deploying & Managing 3D
                Assets
              </div>
              <div className='zoom-counter__text'>
                <p>
                  Enabling businesses to take advantage of 3D commerce and
                  immersive experience by developing 3D content, managing and
                  deploying it on their platforms
                </p>
              </div>
            </div>
            <div className='modelViewer'>
              <model-viewer
                id='reveal'
                src={PanaromaChair}
                alt='A 3D Representation'
                camera-controls
                background-color='white'
                ar
                interaction-policy='allow-when-focused'
                shadow-intensity='1'
                stage-light-intensity='1'
                environment-intensity='1'
                environment-image={backGroundImage}
                camera-orbit='10deg 75deg 30m auto'
                sku='100740'
                auto-rotate='1'
                exposure='1.25'
                onload='handleZindex()'
                class='modelViewer'
                style={{ 'z-index': 20, backgroundColor: '#F4F4F4' }}
              ></model-viewer>

              {/* <ReactPannellum
                id="2"
                sceneId="f2irstScene"
                imageSource={PanaromaImage}
                config={config}
                style={{
                  width: "100%",
                  height: "550px",
                  background: "#000000",
                  position: "relative",
                }}
              /> */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
