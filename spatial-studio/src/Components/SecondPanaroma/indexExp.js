import React from "react";
import { OBJModel } from "react-3d-viewer";

export default () => {
  const modelPath =
    "https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/Duck/glTF/Duck.gltf";
  const realPath =
    "https://cdn.glitch.com/96c6905e-6b45-4288-a0a8-209ae58b3bea%2F2710763_LD.glb?v=1593586917200";

  return (
    <div>
      <OBJModel src='./aaa.obj' />
    </div>
  );
};

// Copy code
