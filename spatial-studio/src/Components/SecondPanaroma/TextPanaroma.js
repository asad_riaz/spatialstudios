import React from 'react';
import styled from 'styled-components';
import './styles.css';
import headerFastCompany from '../../assets/images/headerFastCompany.png';
export const StyledTextPanaroma = styled.div`
  width: 100%;
  background: transparent;
  position: absolute;
  font-size: 140px;
  color: white;
  top: 0%;
  left: 0%;
  .fixed-imge {
    position: absolute;
    top: 35%;
    right: 0px;
  }
`;
export const TextPanaroma = () => {
  return (
    <StyledTextPanaroma>
      <div className='zoom-counter' style={{ position: 'relative' }}>
        <div className='container-fluid'>
          <div className='zoom-counter__inner zoom-counter__inner--first zoom-counter__inner--tab'>
            <div className='zoom-counter__info'>
              <div className='section__label'>
                <span className='label label-default section__label--label'>
                  Our Approach
                </span>
              </div>
              <div className='zoom-counter__title' style={{ color: '#242323' }}>
                The Experience Delivery.
              </div>
              <div className='zoom-counter__text'>
                <p>
                  We leverage our proprietary machine vision algorithms to
                  understand the physical world's context to measure the space
                  and detect any physical objects for optimal layout and
                  placement in augmented reality.{' '}
                </p>
              </div>
            </div>

            <model-viewer
              id='reveal'
              src='https://cdn.glitch.com/96c6905e-6b45-4288-a0a8-209ae58b3bea%2F2710763_LD.glb?v=1593586917200'
              alt='A 3D Representation'
              camera-controls
              background-color='white'
              ar
              shadow-intensity='1'
              stage-light-intensity='0'
              environment-intensity='0'
              camera-orbit='10deg 75deg 2m auto'
              sku='100740'
              exposure='0.75'
              onload='handleZindex()'
              className='modelViewer'
              style={{ 'z-index': 20, 'background-color': '#F4F4F4' }}
            ></model-viewer>
          </div>
        </div>
      </div>
    </StyledTextPanaroma>
  );
};
