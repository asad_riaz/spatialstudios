import React, { useState, useEffect } from "react";
import HeaderMenuItem from "./HeaderMenuItem";

// revirw => what our customers say
// solutions => slider
// portfolio => panaroma one image
const HeaderMenu = () => {
  const [active, setActive] = useState("/");
  const setActiveClass = (header_name) => setActive(header_name);
  return (
    <div className="header-fixed">
      <div className="header-fixed__bottom container">
        <ul className="top-menu header-fixed__menu">
          <HeaderMenuItem
            id="whoWeAre"
            header_name="What_We_Do"
            active={active}
            setActiveClass={setActiveClass}
          >
            What We Do
          </HeaderMenuItem>
          <HeaderMenuItem
            id="whoWeAre"
            header_name="Solutions"
            active={active}
            setActiveClass={setActiveClass}
          >
            Solutions
          </HeaderMenuItem>
          <HeaderMenuItem
            id="scrollToProjects"
            header_name="Portfolio"
            active={active}
            setActiveClass={setActiveClass}
          >
            Portfolio
          </HeaderMenuItem>
          <HeaderMenuItem
            id="scrollToProjects"
            header_name="Reviews"
            active={active}
            setActiveClass={setActiveClass}
          >
            Reviews
          </HeaderMenuItem>
          <HeaderMenuItem
            setActiveClass={setActiveClass}
            header_name="Contact"
            id="scrollToProjects"
            active={active}
          >
            Contact us
          </HeaderMenuItem>
        </ul>
      </div>
    </div>
  );
};

export default HeaderMenu;
