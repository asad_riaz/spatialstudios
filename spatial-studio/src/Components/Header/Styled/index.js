const { default: styled } = require("styled-components");

import styled from "styled-components";

export const LogoImage = styled.img`
  &.logo .header__logo .white_Icon {
    background-image: url(${(p) => (p.src ? p.src : "")});
  }
  &.logo .header__logo .black_Icon {
    background-image: url(${(p) => (p.src ? p.src : "")});
  }
`;
