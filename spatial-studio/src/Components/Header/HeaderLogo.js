import React from 'react';
import './style.css';
import {
  Spatial_Studio_Icon,
  Spatila_Studio_colored_logo_mark,
} from 'assets/images/index';
import ScrollIntoView from 'react-scroll-into-view';
const HeaderLogo = () => {
  return (
    <div className='header__wrapper container-fluid'>
      <ScrollIntoView selector={`#header`} smooth={true}>
        <div className='header__inner'>
          <img
            href='favicon.svg'
            className='logo header__logo white_Icon'
            src={Spatial_Studio_Icon}
            role='button'
          />

          <img
            href='favicon.svg'
            className='logo header__logo black_Icon'
            src={Spatila_Studio_colored_logo_mark}
            role='button'
          />

          <button className='header__menu-button' type='button'>
            <span className='header__menu-button-inner'></span>
          </button>
        </div>
      </ScrollIntoView>
    </div>
  );
};

export default HeaderLogo;
