import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.css";
import HeaderMenu from "./HeaderMenu";
import HeaderLogo from "./HeaderLogo";
const Header = ({ setSelector }) => {
  return (
    <header
      className="header header_white header_fixed header_fixed_top"
      id="header"
      style={{ position: "sticky" }}
    >
      <div className="header__container">
        <HeaderLogo />
        <HeaderMenu setSelector={setSelector} />
      </div>
      {/* <div className="header__overlay"></div>
      <div className="menu-panel header__menu" id="test">
        <div className="menu-panel__inner">
          <button
            className="header__menu-button header__menu-button_fixed"
            type="button"
          >
            <span className="header__menu-button-inner"></span>
          </button>

          <div className="menu-panel__menu">
            <div className="menu-panel__menu-item">
              <a
                className="menu-panel__menu-link menu-panel__menu-link collapsed"
                href="#"
                id="whoWeAre1"
              >
                Solutions
              </a>
            </div>
            <div className="menu-panel__menu-item">
              <a
                className="menu-panel__menu-link collapsed"
                id="scrollToProjects2"
                href="#"
              >
                Portfolio
              </a>
            </div>
            <div className="menu-panel__menu-item">
              <a
                className="menu-panel__menu-link collapsed collapsed"
                type="button"
                data-toggle="modal"
                data-target="#myModal"
                href="#"
              >
                <button
                  type="button"
                  className="btn btn-outline-secondary call-to-action nav-cta-text"
                >
                  Contact Us
                </button>
              </a>
            </div>
          </div>
          <div className="menu-panel__footer">
            <div className="socials menu-panel__socials">
              <a className="socials__social icofont-twitter" href="#">
                <div className="visually-hidden">twitter</div>
              </a>
              <a className="socials__social icofont-facebook" href="#">
                <div className="visually-hidden">facebook</div>
              </a>
              <a className="socials__social icofont-google-plus" href="#">
                <div className="visually-hidden">google plus</div>
              </a>
            </div>
            <div className="menu-panel__bottom">
              <div className="menu-panel__copyright">
                © 2020
                <strong>VISIONX.</strong>
                All Rights Reserved.
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </header>
  );
};

export default Header;
