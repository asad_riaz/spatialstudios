import React from 'react';
import rightIcon from '../../assets/images/m.svg';
import ScrollIntoView from 'react-scroll-into-view';

const HeaderMenuItem = ({ active, header_name, setActiveClass, children }) => {
  const activeclass = 'btn btn-outline-secondary call-to-action scroll-button';
  return (
    <li
      className='top-menu__menu-item'
      onClick={() => setActiveClass(header_name)}
    >
      <div className='dropdown'>
        <ScrollIntoView selector={`#${header_name}`} smooth={true}>
          <button
            className={`dropdown__trigger top-menu__menu-link call-button ${
              header_name === 'Contact' ? activeclass : ''
            } `}
            href='#'
            id='whoWeAre'
          >
            {children}
            {header_name === 'Contact' && active ? (
              <span>
                <img className='gutterArrow' src={rightIcon} alt='icon' />
              </span>
            ) : null}
          </button>{' '}
        </ScrollIntoView>
      </div>
    </li>
  );
};

export default HeaderMenuItem;
