import React from 'react';
import './styles.css';
import YellowLicon from 'assets/images/a.svg';
import TestimonialIcon from '../../assets/images/testimonial.png';
import TestimonialImage from '../../assets/images/WasifAwan.png';
// import TestimonialIcon from '../../assets/images/testimonial.png';

const testimonial = () => {
  return (
    <div className='zoom-counter testimonial-section' id='Reviews'>
      <div className='L__icon--yellow'>
        <img src={YellowLicon} alt='Licon' />
      </div>
      <div className='testimonial-container'>
        <div className='testimonial__label'>
          <span className='label label-default testimonial__label--label'>
            Testimonials
          </span>
        </div>
        <div className='testimonial__title'>What Our Clients Say</div>
        <div className='testimonial__content'>
          We're committed to enabling business to seamlessly deploy immersive
          experiences by giving them the ability to develop, deploy and manage
          3D assets
        </div>
      </div>

      <div className='container'>
        <div className='testimonial-cards'>
          <div className='testimonial-card'>
            <img
              src={TestimonialIcon}
              alt='testimonial'
              className='testimonial__icon'
            />
            <div className='testimonial-card-content'>
              VisionX has been a critical Immersive Technology partner for
              Staples. They integrate with our teams
            </div>

            <div className='testimonial__avatar'>
              <img
                className='testimonial__avatar--img'
                src={TestimonialImage}
                alt='Testimonialimage'
              />
              {/* <div class='avatar'></div> */}
              <div className='testimonial__names'>
                <div className='testimonial__name'>Wasif Awan </div>
                <div className='testimonial__post'>
                  Vice President Ecommerce Engineering at Staples
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default testimonial;
