import React from 'react';
import ReactPannellum, { getConfig } from 'react-pannellum';
import PanaromaImage from 'assets/panaromas/panorama1.jpg';
import { TextPanaroma } from './TextPanaroma';
export default class Example extends React.Component {
  click() {
    console.log(getConfig());
  }

  render() {
    const config = {
      autoRotate: -4,
      autoLoad: true,
    };
    return (
      <div>
        <ReactPannellum
          id='1'
          sceneId='firstScene'
          imageSource={PanaromaImage}
          config={config}
          style={{
            width: '100%',
            height: '89vh',
            background: '#000000',
            position: 'relative',
          }}
        />
        <TextPanaroma>Click me</TextPanaroma>
      </div>
    );
  }
}
