import React from 'react';
import styled from 'styled-components';
import rightIcon from '../../assets/images/l.svg';
import headerFastCompany from '../../assets/images/fastt.svg';
import Fast2 from '../../assets/images/headerFast.svg';
import ScrollIntoView from 'react-scroll-into-view';
export const StyledTextPanaroma = styled.div`
  width: 100%;
  background: transparent;
  position: absolute;
  font-size: 140px;
  color: white;
  top: 90px;
  left: 0%;
  .fixed-imge {
    position: absolute;
    top: 21%;
    right: 0px;
  }
`;
export const TextPanaroma = () => {
  return (
    <StyledTextPanaroma>
      <div className='panorama-slider2__slide '>
        <div className='container-fluid'>
          <div className='panorama-slider2__slide-inner'>
            <div className='panorama-slider2__title-wrapper'>
              <div
                className='panorama-slider2__title zoom-image-head__title'
                data-frames='[{"delay":200,"speed":500,"frame":"0","from":"x:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"x:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
                data-height='none'
                data-responsive_offset='on'
                data-type='text'
                data-whitespace='nowrap'
                data-width='none'
              >
                Bring Products to life with 3D & Immersive Experiences
              </div>
              <div className='fixed-imge'>
                <picture>
                  <source
                    media='(min-width: 600px)'
                    srcset={headerFastCompany}
                  />
                  <source media='(min-width: 300px)' srcset={Fast2} />
                  <img
                    class='image-fast'
                    src={headerFastCompany}
                    alt='Fast Company'
                    style={{ position: 'sticky' }}
                  />
                </picture>
              </div>
            </div>

            <div
              className='zoom-image-head__subtitle'
              data-frames='[{"delay":400,"speed":500,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"frame":"999","to":"y:-50px;opacity:0;","ease":"Power3.easeInOut"}]'
              data-height='none'
              data-responsive_offset='on'
              data-type='text'
              data-whitespace='nowrap'
              data-width='none'
            >
              Democratize 3D content to unlock higher customer engagement &
              increased conversion
            </div>

            <ScrollIntoView selector={`#Contact`} smooth={true}>
              <button href='#' className='btn cta'>
                <span style={{ position: 'relative', top: '-5px' }}>
                  <span
                    style={{
                      fontSize: '1.9rem',
                      position: 'relative',
                      top: '-5px',
                      fontWeight: '700',
                      lineHeight: '1.579',
                    }}
                  ></span>
                  Book a Demo{' '}
                  <span>
                    <img
                      className='gutterArrow'
                      src={rightIcon}
                      alt='rightIcon'
                    />
                  </span>
                </span>
              </button>
            </ScrollIntoView>
          </div>
        </div>
      </div>
    </StyledTextPanaroma>
  );
};
