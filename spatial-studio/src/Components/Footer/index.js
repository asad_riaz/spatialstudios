import React from "react";
import {
  StyledFooter,
  LeftPortion,
  RightPortion,
  FooterCopyRight,
  FooterLogo,
  FooterMenu,
  FooterMenuItem,
  FooterIcon,
  FooterIconMenu,
  TopFooter,
  BottomFooter,
} from "./Styled";
import LogoImage from "assets/images/Spatial_Studio_Icon.svg";
import Twitter from "assets/images/twitter.png";
import Insta from "assets/images/instagram.png";
import Facebook from "assets/images/facebook.png";
import LinkedIn from "assets/images/linkedin.png";
const Footer = () => {
  return (
    <div className="container">
      <StyledFooter>
        <div style={{ width: "100%" }}>
          <TopFooter>
            <FooterLogo src={LogoImage} />
            <FooterMenu>
              <FooterMenuItem>Contact</FooterMenuItem>
              <FooterMenuItem>Careers</FooterMenuItem>
              <FooterMenuItem>Term & Conditions</FooterMenuItem>
              <FooterMenuItem>Privacy</FooterMenuItem>
            </FooterMenu>
          </TopFooter>
          {/* <LeftPortion></LeftPortion> */}
          <BottomFooter>
            <FooterIconMenu>
              {/* <FooterIcon src={Facebook} />
              <FooterIcon src={Twitter} />
              <FooterIcon src={LinkedIn} />
              <FooterIcon src={Insta} /> */}
            </FooterIconMenu>
            <FooterCopyRight>
              © 2020 VisionX Technologies, Inc. All rights reserved.
            </FooterCopyRight>
          </BottomFooter>
        </div>
      </StyledFooter>
    </div>
  );
};

export default Footer;
