import styled from "styled-components";

export const StyledFooter = styled.footer`
  color: black !important;
  width: 100%;
  /* background: yellow; */
  height: 280px;
  box-sizing: border-box;
  /* padding: 30px 50px; */
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 420px) {
    height: 400px;
  }
  @media (max-width: 720px) {
    height: 400px;
  }
  /* flex-wrap: wrap; */
`;
export const FooterMenu = styled.ul`
  list-style-type: none;
  margin: 0;
  padding: 0;
  @media (max-width: 420px) {
    margin-top: 20px;
  }
`;
export const FooterIconMenu = styled.ul`
  list-style-type: none;
  display: flex;
  justify-content: flex-end;
  margin: 0;
  padding: 0;
`;

export const TopFooter = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;
export const BottomFooter = styled.div`
  display: flex;
  flex-direction: row-reverse;
  justify-content: space-between;
  flex-wrap: wrap;I
`;
export const FooterIcon = styled.li`
  position: relative;
  display: inline-block;
  cursor: pointer;
  margin: 0 10px;
  ::after {
    content: "";
    width: 50px;
    height: 50px;
    display: inline-block;
    /* position: absolute; */
    background-image: url(${(p) => (p.src ? p.src : "")});
    background-color: transparent;
    background-position: center;
    background-repeat: no-repeat;
  }
`;
export const FooterMenuItem = styled.li`
  display: inline-block;
  text-transform: uppercase;
  letter-spacing: 0.7px;
  font-size: 16px;
  font-weight: 600;
  height: 40px;
  margin: 0 20px;
  @media (max-width: 1000px) {
    display: block;
    margin: 5px;
  }

  /* width: 140px; */
`;
export const FooterLogo = styled.div`
  background-image: url(${(p) => (p.src ? p.src : "")});
  width: 190px;
  height: 55px;
  background-color: transparent;
  background-position: center;
  background-repeat: no-repeat;
`;
export const FooterCopyRight = styled.div`
  letter-spacing: 0.7px;
  color: #252e3b;
  margin-top: 20px;
`;
