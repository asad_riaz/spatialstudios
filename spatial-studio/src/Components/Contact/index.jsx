import React, { useState } from 'react';
import emailjs from 'emailjs-com';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import apiKeys from '../../apiKeys.js';
import './style.css';
import redIcon from 'assets/images/rotatedLiconred.svg';
import LocationIcon from '../../assets/images/footerlocatiob.svg';
import rightIcon from 'assets/images/m.svg';
// import ContactIcon from '../../assets/images/footerphone.svg';

const Contact = () => {
  const [emailSent, setEmailSent] = useState(false);
  // const notify = () => toast('Wow so easy !');
  const onSubmit = (e) => {
    // alert('Hello');
    e.preventDefault(); // Prevents default refresh by the browser
    toast.success("We 'll get back to you shortly", {
      position: 'top-right',
      autoClose: 3000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

    emailjs
      .sendForm(
        'contact_service',
        apiKeys.TEMPLATE_ID,
        e.target,
        apiKeys.USER_ID,
      )
      .then(
        (result) => {
          // alert("Message Sent, I'll get back to you shortly", result.text);
          window.location.reload(false);
        },
        (error) => {
          alert('An error occured, Plese try again', error.text);
        },
      );
  };

  return (
    <div className='contactUs-section' style={{ position: 'relative' }}>
      <div className='img-background_contact--rect'>
        <img src={redIcon} alt='bg' />
      </div>
      <div className='contact-content container-fluid'>
        <div className='contact-info'>
          <div className='contact__label' id='Contact'>
            <span className='label label-default contact__label--label'>
              Contact Us
            </span>
          </div>
          <div className='contact__title'>Reach Out to Us for Any Inquiry</div>
          <div className='contact__location'>
            <img
              src={LocationIcon}
              alt='location'
              className='contact__location--icon'
            />
            <div className='contact__location--address contact-text'>
              <div className='contact__location--street'>500 7th Ave</div>
              <div className='contact__location--city'>New York, NY 10018</div>
            </div>
          </div>
          {/* <div className='contact__phone'>
            <img
              src={ContactIcon}
              alt='contact'
              className='contact__phone--icon'
            />
            <div className='contact__phone--number contact-text'>
              (+0084) 912-3548-073
            </div>
          </div> */}
        </div>
        <div className='contact-form'>
          <form className='form' onSubmit={onSubmit}>
            <div className='input__text'>
              <label className='input__text--label'>Name</label>
              <input
                className='input__text--input'
                type='text'
                name='name'
                id='name'
                autoComplete='off'
              />
            </div>
            <div className='input__text gutter-top '>
              <label className='input__text--label'>Email</label>
              <input
                className='input__text--input'
                type='email'
                name='email'
                id='email'
                autoComplete='off'
              />
            </div>
            <div className='input__text gutter-top input__text--textbox'>
              <label className='input__textarea--label'>Message</label>
              <textarea
                className='input__text--textarea'
                id='w3review'
                name='message'
                rows='20'
                cols='50'
              ></textarea>
            </div>

            <div className='butn'>
              {/* footer-studio__submit */}
              <button
                type='submit'
                class='send__email'
                id='sendEmail'
                // onClick={notify}
              >
                Submit{' '}
                <span style={{ padding: '0px 0px 0px 22px' }}>
                  <img
                    src={rightIcon}
                    alt='icon'
                    style={{ marginTop: '-2px' }}
                  />
                </span>
                <ToastContainer />
                {/* <span class='footer-studio__submit-icon icon-chevron-right'></span> */}
                <div
                  class='spinner footer-studio__loading-icon'
                  id='footer_loader'
                >
                  <div class='double-bounce1'></div>
                  <div class='double-bounce2'></div>
                </div>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Contact;
