import React from 'react';
import './style.css';
import EMAAR from 'assets/images/hh.svg';
import Staples from 'assets/images/ss.svg';
import RH from 'assets/images/rh.svg';
import Hanna from 'assets/images/ha.svg';
import Movado from 'assets/images/MOVADO.svg';
import New_balance from 'assets/images/nb.svg';
import Rectangle from 'assets/images/Rectangle.svg';
const ClientsInfo = () => {
  return (
    <div
      className='container-fluid'
      style={{ background: '#F7F8F9', position: 'relative' }}
    >
      <div className='img-background_clients--rect'>
        <img src={Rectangle} alt='bg' />
      </div>
      <div className='container section-clients'>
        <div className='clients'>
          <div className='clients__list'>
            <div className='clients__item'>
              <div className='clients__item-inner'>
                <img alt='' src={RH} />
              </div>
            </div>
            <div className='clients__item'>
              <div className='clients__item-inner'>
                <img alt='' src={EMAAR} />
              </div>
            </div>
            <div className='clients__item'>
              <div className='clients__item-inner'>
                <img alt='' src={Staples} />
              </div>
            </div>

            <div className='clients__item'>
              <div className='clients__item-inner'>
                <img alt='' src={Movado} />
              </div>
            </div>
            <div className='clients__item'>
              <div className='clients__item-inner'>
                <img alt='' src={Hanna} />
              </div>
            </div>
            <div className='clients__item'>
              <div className='clients__item-inner'>
                <img alt='' src={New_balance} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ClientsInfo;
