import React from 'react';
import SliderImage1 from 'assets/images/3D as a service.png';
import clsx from 'clsx';

class TestSlider extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imgClasses: '',
    };
    this.exampleRef = React.createRef();
  }

  componentDidMount = () => {
    this.exampleRef.current.classList.contains('active')
      ? this.setState({ imgClasses: 'd-block' })
      : this.setState({ imgClasses: 'd-none' });
  };

  test = () => {};

  render() {
    const { index, classes } = this.props;

    const carouselClass =
      index === 1 ? 'carousel-item active' : 'carousel-item';
    // const dNone = classes === 'carousel-item active' ? 'd-none' : '';
    return (
      <>
        <div
          className={carouselClass}
          ref={this.exampleRef}
          data-interval='10000'
        >
          <div class='carousel-caption d-flex align-items-center p-5'>
            <div class='text-left'>
              <h5>First slide label</h5>
              <p>
                Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                Accusamus, veritatis ipsam temporibus amet, accusantium nulla
              </p>
            </div>
          </div>
        </div>
        <img
          src={SliderImage1}
          className={clsx('image', true && this.state.imgClasses)}
          alt='...'
        />
      </>
    );
  }
}

export default TestSlider;
