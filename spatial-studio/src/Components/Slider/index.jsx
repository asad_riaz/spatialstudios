import React from 'react';
import './style.css';
import './responsive.css';
import SliderImage1 from 'assets/images/1 small.svg';
import SliderImage2 from 'assets/images/2.svg';
import SliderImage3 from 'assets/images/3.svg';
import SliderImage4 from 'assets/images/4.svg';
import SliderImage5 from 'assets/images/5.svg';
import Rectangle from 'assets/images/Rectangle.svg';
import icon from 'assets/images/Group 4.png';

const slider = () => {
  return (
    <div className='zoom-counter solution-section' id='Solutions'>
      <div className='img-background_solution'>
        <img src={Rectangle} alt='bg' />
      </div>
      <div className='img-background_solution--square'>
        <img src={icon} alt='bg' />
      </div>
      <div className='solution-container'>
        <div className='solution__label'>
          <span className='label label-default solution__label--label'>
            Our Solutions
          </span>
        </div>
        <div className='solution__title'>
          Bringing your digital stores to the physical world
        </div>
        <div className='solution__content'>
          From Content creation to integration to immersive experience
          deployment, we do it all
        </div>
      </div>
      <div
        id='carouselExampleInterval'
        class='carousel slide d-flex justify-content-end'
        data-ride='carousel'
      >
        <ol class='carousel-indicators'>
          <li
            data-target='#carouselExampleInterval'
            data-slide-to='0'
            class='active'
          ></li>
          <li data-target='#carouselExampleCaptions' data-slide-to='1'></li>
          <li data-target='#carouselExampleCaptions' data-slide-to='2'></li>
          <li data-target='#carouselExampleCaptions' data-slide-to='3'></li>
          <li data-target='#carouselExampleCaptions' data-slide-to='4'></li>
        </ol>
        <div class='carousel-inner'>
          <div class='carousel-item active' data-interval='10000'>
            <img src={SliderImage1} class='image' alt='...' />

            <div class='carousel-caption d-flex align-items-center p-5'>
              <div class='text-left'>
                <h5>3D-as-a-Service</h5>
                <p>
                  3D modeling stays at the core of building immersive and
                  spatial experiences. We leverage available digital assets (2D
                  images, CAD files) and emerging technologies (LIDAR) to
                  deliver 3D models at scale using our proprietary
                  semi-automated 3D-as-a-Service development pipeline.
                </p>
              </div>
            </div>
          </div>
          <div class='carousel-item' data-interval='10000'>
            <img src={SliderImage2} class='image' alt='...' />

            <div class='carousel-caption d-flex align-items-center p-5'>
              <div class='text-left'>
                <h5>Digital Asset Management</h5>
                <p>
                  The developed 3D assets are managed and optimized for
                  cross-platform augmented reality and immersive experiences
                  rendered directly from our cloud to your customers for
                  marketing, sales, and servicing.
                </p>
              </div>
            </div>
          </div>

          <div class='carousel-item' data-interval='10000'>
            <img src={SliderImage3} class='image' alt='...' />

            <div class='carousel-caption d-flex align-items-center p-5'>
              <div class='text-left'>
                <h5>Space Intelligence</h5>
                <p>
                  We leverage our proprietary machine vision algorithms to
                  understand the physical world's context to measure the space
                  and detect any physical objects for optimal layout and
                  placement in augmented reality.
                </p>
              </div>
            </div>
          </div>
          <div class='carousel-item' data-interval='10000'>
            <img src={SliderImage4} class='image' alt='...' />

            <div class='carousel-caption d-flex align-items-center p-5'>
              <div class='text-left'>
                <h5>Experience Delivery</h5>
                <p>
                  The integrated output of 3D-as-a-Service, Digital Asset
                  Management, and Space Intelligence powers the existing or
                  purpose-built front-end applications, be it desktop, mobile,
                  or next-gen wearables.
                </p>
              </div>
            </div>
          </div>
          <div class='carousel-item' data-interval='10000'>
            <img src={SliderImage5} class='image' alt='...' />

            <div class='carousel-caption d-flex align-items-center p-5'>
              <div class='text-left'>
                <h5>Analytics</h5>
                <p>
                  We provide in-depth analytics on your customer engagements
                  that help businesses understand assortment curation and offer
                  smart recommendations.
                </p>
              </div>
            </div>
          </div>
        </div>

        <a
          class='carousel-control-prev'
          href='#carouselExampleInterval'
          role='button'
          data-slide='prev'
        >
          <div class='control-size'>
            <i class='fa fa-angle-left' aria-hidden='true'></i>
          </div>

          <span class='sr-only'>Previous</span>
        </a>
        <a
          class='carousel-control-next'
          href='#carouselExampleInterval'
          role='button'
          data-slide='next'
        >
          <div class='control-size'>
            <i class='fa fa-angle-right' aria-hidden='true'></i>
          </div>
          <span class='sr-only'>Next</span>
        </a>
      </div>
    </div>
  );
};

export default slider;
